var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Address
var Address = new Schema({
  street: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  city: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  state: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  zipCode: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
},{
    collection: 'addresss'
});

module.exports = mongoose.model('Address', Address);