var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Company
var Company = new Schema({
  name: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  establishedOn: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  revenue: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  Employees: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  Departments: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  Divisions: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  BoardMembers: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  Address: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  Type: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  Industry: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
},{
    collection: 'companys'
});

module.exports = mongoose.model('Company', Company);