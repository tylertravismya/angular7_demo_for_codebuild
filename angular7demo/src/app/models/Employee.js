var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Employee
var Employee = new Schema({
  firstName: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  lastName: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
  Type: {
#attributeTypeDeclaration(${attribute}, ${classObject}, ${outputTheAttributeType})
  },
},{
    collection: 'employees'
});

module.exports = mongoose.model('Employee', Employee);