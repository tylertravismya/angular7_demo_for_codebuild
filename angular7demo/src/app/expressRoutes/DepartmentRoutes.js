// departmentRoutes.js

var express = require('express');
var app = express();
var departmentRoutes = express.Router();

// Require Item model in our routes module
var Department = require('../models/Department');

// Defined store route
departmentRoutes.route('/add').post(function (req, res) {
	var department = new Department(req.body);
	department.save()
    .then(item => {
    	res.status(200).json({'department': 'Department added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
departmentRoutes.route('/').get(function (req, res) {
	Department.find(function (err, departments){
		if(err){
			console.log(err);
		}
		else {
			res.json(departments);
		}
	});
});

// Defined edit route
departmentRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Department.findById(id, function (err, department){
		res.json(department);
	});
});

//  Defined update route
departmentRoutes.route('/update/:id').post(function (req, res) {
	Department.findById(req.params.id, function(err, department) {
		if (!department)
			return next(new Error('Could not load a Department Document using id ' + req.params.id));
		else {
            department.name = req.body.name;
            department.Head = req.body.Head;

			department.save().then(department => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
departmentRoutes.route('/delete/:id').get(function (req, res) {
   Department.findOneAndDelete({_id: req.params.id}, function(err, department){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Department + ' using id ' + req.params.id );
    });
});

module.exports = departmentRoutes;