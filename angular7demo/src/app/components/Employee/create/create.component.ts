import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../../services/Employee.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Employee/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateEmployeeComponent extends SubBaseComponent implements OnInit {

  title = 'Add Employee';
  employeeForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private employeeservice: EmployeeService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.employeeForm = this.fb.group({
#outputDataValidators()    
   });
  }
  addEmployee(firstName, lastName, Type) {
      this.employeeservice.addEmployee(firstName, lastName, Type)
      	.then(success => this.router.navigate(['/indexEmployee']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
