import { HttpClient } from '@angular/common/http';
import * as enumTypes from '../models/EnumTypes';

import {AddressService} from '../services/Address.service';
import {CompanyService} from '../services/Company.service';
import {DepartmentService} from '../services/Department.service';
import {DivisionService} from '../services/Division.service';
import {EmployeeService} from '../services/Employee.service';

/** 
	Base class of all Components.  
	For convenience, contains all enums and entity lists 
**/
export class BaseComponent {

    constructor (private http: HttpClient) {}

// enum instances
    CompanyTypes = Object.keys(enumTypes.CompanyType);
    EmploymentTypes = Object.keys(enumTypes.EmploymentType);
    Industrys = Object.keys(enumTypes.Industry);

// all collection instances
    addresss : any;
    companys : any;
    departments : any;
    divisions : any;
    employees : any;
  
// initialization  
    ngOnInit() {
    }

    initAddressList() {
        if ( this.addresss == null ) {
            new AddressService(this.http).getAddresss().subscribe(res => {
                this.addresss = res;
            });
        }
    }
    
    initCompanyList() {
        if ( this.companys == null ) {
            new CompanyService(this.http).getCompanys().subscribe(res => {
                this.companys = res;
            });
        }
    }
    
    initDepartmentList() {
        if ( this.departments == null ) {
            new DepartmentService(this.http).getDepartments().subscribe(res => {
                this.departments = res;
            });
        }
    }
    
    initDivisionList() {
        if ( this.divisions == null ) {
            new DivisionService(this.http).getDivisions().subscribe(res => {
                this.divisions = res;
            });
        }
    }
    
    initEmployeeList() {
        if ( this.employees == null ) {
            new EmployeeService(this.http).getEmployees().subscribe(res => {
                this.employees = res;
            });
        }
    }
    
    
// comparison function for select controls  
    compareFn(user1: any, user2: any) {
        return user1 == user2
    }    
}
