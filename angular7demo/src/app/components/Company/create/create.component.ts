import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../../../services/Company.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Company/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateCompanyComponent extends SubBaseComponent implements OnInit {

  title = 'Add Company';
  companyForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private companyservice: CompanyService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.companyForm = this.fb.group({
#outputDataValidators()    
   });
  }
  addCompany(name, establishedOn, revenue, Employees, Departments, Divisions, BoardMembers, Address, Type, Industry) {
      this.companyservice.addCompany(name, establishedOn, revenue, Employees, Departments, Divisions, BoardMembers, Address, Type, Industry)
      	.then(success => this.router.navigate(['/indexCompany']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
