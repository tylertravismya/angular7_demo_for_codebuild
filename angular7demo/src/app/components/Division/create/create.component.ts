import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DivisionService } from '../../../services/Division.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Division/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateDivisionComponent extends SubBaseComponent implements OnInit {

  title = 'Add Division';
  divisionForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private divisionservice: DivisionService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.divisionForm = this.fb.group({
#outputDataValidators()    
   });
  }
  addDivision(name, Head) {
      this.divisionservice.addDivision(name, Head)
      	.then(success => this.router.navigate(['/indexDivision']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
