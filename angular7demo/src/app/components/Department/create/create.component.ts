import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DepartmentService } from '../../../services/Department.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Department/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateDepartmentComponent extends SubBaseComponent implements OnInit {

  title = 'Add Department';
  departmentForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private departmentservice: DepartmentService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.departmentForm = this.fb.group({
#outputDataValidators()    
   });
  }
  addDepartment(name, Head) {
      this.departmentservice.addDepartment(name, Head)
      	.then(success => this.router.navigate(['/indexDepartment']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
