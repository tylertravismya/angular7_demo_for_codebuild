import { DepartmentService } from '../../../services/Department.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Department } from '../../../models/Department';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexDepartmentComponent implements OnInit {

  departments: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: DepartmentService) {}

  ngOnInit() {
    this.getDepartments();
  }

  getDepartments() {
    this.service.getDepartments().subscribe(res => {
      this.departments = res;
    });
  }

  deleteDepartment(id) {
    this.service.deleteDepartment(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexDepartment']));
			});  }
}
