import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DepartmentService } from '../../../services/Department.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Department/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditDepartmentComponent extends SubBaseComponent implements OnInit {

  department: any;
  departmentForm: FormGroup;
  title = 'Edit Department';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: DepartmentService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.departmentForm = this.fb.group({
#outputDataValidators()
   });
  }
  updateDepartment(name, Head) {
    this.route.params.subscribe(params => {
    	this.service.updateDepartment(name, Head, params['id'])
      		.then(success => this.router.navigate(['/indexDepartment']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.department = this.service.editDepartment(params['id']).subscribe(res => {
        this.department = res;
      });
    });
    
    super.ngOnInit();
  }
}
